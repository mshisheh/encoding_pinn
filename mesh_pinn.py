#

from network import DNN
import torch
import numpy as np
from encoding import Mesh_encoding


if torch.cuda.is_available():
    device = torch.device('cuda')
    torch.set_default_tensor_type('torch.cuda.FloatTensor')
else:
    device = torch.device('cpu')


class Mesh_PINN():
    def __init__(self, args) -> None:
        
        self.args = args
        if args.encoding:
            self.encoding = Mesh_encoding(args)
            self.input_dim = self.args.F
        else:
            self.input_dim = 2.0

        xmin, xmax, ymin, ymax = args.domain_size
        ub = np.array([xmax, ymax])
        lb = np.array([xmin, ymin])

        self.lr = args.lr
        self.set_optimizer()
        self.network = DNN(dim_in=self.input_dim, dim_out=1, n_layer=2, n_node= 16, ub = ub, lb=lb, activation=torch.nn.Tanh()).to(device)
        self.dnn = torch.nn.Sequential(self.encoding, self.network)


    def set_optimizer(self):
        if self.optim == 'lbfgs':
            self.optimizer = torch.optim.LBFGS(
                self.dnn.parameters(), 
                lr=self.lr, 
                #max_iter=self.max_iter, 
                #max_eval=50000,
                #history_size=50,
                #tolerance_grad=1e-6,
                #tolerance_change=1.0 * np.finfo(float).eps,
                line_search_fn="strong_wolfe"       # can be "strong_wolfe"
            )
        elif self.optim == 'adam':
            self.optimizer = torch.optim.Adam(self.dnn.parameters(), lr = self.lr)
        else:
            raise NotImplementedError()

    def train(self):
        pass

    def test(self):
        pass

    

