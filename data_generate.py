import torch
import numpy as np
from scipy.stats import qmc

device = torch.device("cuda:0")

def customized_2d_exact_u(y, x, omega):
    # data
    return torch.sin(omega * x * y) + x**2.0 + 2 * x * y


def generate_Customized_2d_train_data(num_train, num_bc, omega, domain_size):
    # size of the domain
    xmin, xmax, ymin, ymax = domain_size

    # colocation points
    xc = torch.empty((num_train, 1), dtype=torch.float32).uniform_(xmin, xmax).to(device)
    yc = torch.empty((num_train, 1), dtype=torch.float32).uniform_(ymin, ymax).to(device)
  
    # requires grad
    yc.requires_grad = True
    xc.requires_grad = True
    uc = torch.zeros_like(xc).to(device)
    # boundary points
    north = torch.empty((num_bc, 1), dtype=torch.float32).uniform_(xmin, xmax)
    west = torch.empty((num_bc, 1), dtype=torch.float32).uniform_(ymin, ymax)
    south = torch.empty((num_bc, 1), dtype=torch.float32).uniform_(xmin, xmax)
    east = torch.empty((num_bc, 1), dtype=torch.float32).uniform_(ymin, ymax)
    yb = torch.cat([
        torch.ones((num_bc, 1)) * ymax, west,
        torch.ones((num_bc, 1)) * ymin, east
        ]).to(device)
    xb = torch.cat([
        north, torch.ones((num_bc, 1)) * xmin,
        south, torch.ones((num_bc, 1)) * xmax
        ]).to(device)
    ub = customized_2d_exact_u(yb, xb, omega).to(device)
    return yc, xc, uc, yb, xb, ub


def generate_Customized_2d_test_data(num_test, omega, domain_size):

    # size of the domain
    xmin, xmax, ymin, ymax = domain_size
    # test points
    y = torch.linspace(ymin, ymax, num_test).to(device)
    x = torch.linspace(xmin, xmax, num_test).to(device)
    y, x = torch.meshgrid([y, x], indexing='ij')
    y_test = y.reshape(-1, 1)
    x_test = x.reshape(-1, 1)
    u_test = customized_2d_exact_u(y_test, x_test, omega)
    return y_test, x_test, u_test


def generate_Customized_2d_train_data_sobol(num_train, num_bc, omega, domain_size):
    # size of the domain
    xmin, xmax, ymin, ymax = domain_size

    Sobol1d = qmc.Sobol(d = 1, scramble=True)
    Sobol2d = qmc.Sobol(d = 2, scramble=True)

    data = Sobol2d.random(num_train)
    x = data[:,0] * (xmax - xmin) + xmin
    y = data[:,1] * (ymax - ymin) + ymin

    # colocation points
    xc = torch.tensor(x, dtype=torch.float32).to(device).reshape(-1,1)
    yc = torch.tensor(y, dtype=torch.float32).to(device).reshape(-1,1)
  
    # requires grad
    yc.requires_grad = True
    xc.requires_grad = True
    uc = torch.zeros_like(xc).to(device)
    # boundary points
    north = torch.tensor(Sobol1d.random(num_bc) * (xmax - xmin) + xmin, dtype=torch.float32).reshape(-1,1)
    west = torch.tensor(Sobol1d.random(num_bc) * (ymax - ymin) + ymin, dtype=torch.float32).reshape(-1,1)
    south = torch.tensor(Sobol1d.random(num_bc) * (xmax - xmin) + xmin, dtype=torch.float32).reshape(-1,1)
    east = torch.tensor(Sobol1d.random(num_bc) * (ymax - ymin) + ymin, dtype=torch.float32).reshape(-1,1)
    yb = torch.cat([
        torch.ones((num_bc, 1)) * ymax, west,
        torch.ones((num_bc, 1)) * ymin, east
        ]).to(device)
    xb = torch.cat([
        north, torch.ones((num_bc, 1)) * xmin,
        south, torch.ones((num_bc, 1)) * xmax
        ]).to(device)
    ub = customized_2d_exact_u(yb, xb, omega).to(device)
    return yc, xc, uc, yb, xb, ub