import numpy as np
import matplotlib.pyplot as plt

import sys

import torch
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

# Parameters
x_min = -1.0
x_max = 1.0
y_min = 0.0
y_max = 5.0

np.random.RandomState(1235)

x = torch.linspace(x_min,x_max, 250).to(device)
y = torch.linspace(y_min,y_max, 250).to(device)

x1, x2 = torch.meshgrid(x,y)

omega = 1
#_________________________________Analytical___________________________________________
z = torch.sin(omega * (x1 * x2)) + x1**2 + 2 * x1 * x2
zpx1 = x2 * omega * torch.cos(x1 * x2 * omega) + 2 * (x1 + x2)
zpx1x1 =  2 - x2 **2 * omega**2 * torch.sin(x1 * x2 * omega)

zpx2 = x1 * (omega * torch.cos(x2 * x1 * omega) + 2)
zpx2x2 =  -x1**2 * omega**2 * torch.sin(x2 * x1 * omega)
#---------------------------------------------------------------------------

data =  torch.concatenate([x1.reshape(-1,1),x2.reshape(-1,1),z.reshape(-1,1)], axis = -1)

# with open(f'./real_values_{omega}_2.npy', 'wb') as f:
#     np.save(f, data.cpu().numpy())


# data = torch.tensor(np.load(f'./real_values_{omega}_2.npy')).to(device)
#data_mean, data_std = data[:,:-1].mean(), data[:,:-1].std()
#data = (data - data.mean(axis=0))/(data.std(axis = 0))

from rewheel_main4 import PINN
pinn = PINN()
#pinn.load_state_dict(torch.load(f"./best_sofar.pt"))
pinn.load_state_dict(torch.load(f"./last_model_data_{omega}_2d.pt"))
pinn.eval()
#_________________________________U from NN___________________________________________
#xy = torch.tensor(torch.hstack((,)), dtype=torch.float32).to(device)
#print(xy.shape , x1.shape)

#xy = (xy - xy.min(axis = 0)[0])/(xy.max(axis = 0)[0] - xy.min(axis = 0)[0])
data.requires_grad = True
x = torch.tensor(x1, dtype=torch.float32, requires_grad=True).to(device).reshape(-1,1)
y = torch.tensor(x2, dtype=torch.float32, requires_grad=True).to(device).reshape(-1,1)

u = pinn(x, y).detach().reshape(x1.shape)
print(u.shape)#.reshape(x1.shape)

upx1, upx2, uxx, uyy = pinn.derivitives(x, y)
upx1 = upx1.detach().reshape(x1.shape)
upx1x1 = uxx.detach().reshape(x1.shape)

upx2 = upx2.detach().reshape(x1.shape)
upx2x2 = uyy.detach().reshape(x1.shape)

#_________________________________RRMSE___________________________________________
error = torch.sum(((u - z))**2)/u.numel()
error = torch.sqrt(error/torch.sum(u**2))
print(f'RRMSE Error for u is: {error}')
#----------------RMSE zpx1---------------------
error = torch.sum(((upx1 - zpx1))**2)/upx1.numel()
error = torch.sqrt(error/torch.sum(upx1**2))
print(f'RRMSE Error for du/dx1 is: {error}')
#----------------RMSE zpx1---------------------
error = torch.sum(((upx1x1 - zpx1x1))**2)/upx1x1.numel()
error = torch.sqrt(error/torch.sum(upx1x1**2))
print(f'RRMSE Error for d2u/dx2 is: {error}')
#-----------------------------------------------


#_________________________________Plots___________________________________________
from matplotlib.gridspec import GridSpec

def plot2D(x1, x2, y, colormap_min = 0, colormap_max = 0, t='Title'):
    if colormap_max == colormap_min: #for errors where limits change or are unknown
        plt.pcolormesh(x1.cpu(), x2.cpu(), y.cpu(), cmap = 'jet' ,)# .to(device)
        plt.colorbar(format='%.0e')
    else:
        plt.pcolormesh(x1.cpu(), x2.cpu(), y.cpu(), cmap = 'jet', vmin=colormap_min, vmax=colormap_max)
        plt.colorbar()
    
    #plt.xlabel('x')
    #plt.ylabel('y')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)    
    plt.title(t)
    #ax.set_aspect(1)

subplot_size = 3.
(width, height) = (5*subplot_size, 3*subplot_size) #5 col and 3 rows, each of size subplot_size
fig = plt.figure(figsize = (width, height))

plt.rc('font', size=8)          # controls default text sizes
plt.rc('axes', titlesize=8)     # fontsize of the axes title
plt.rc('axes', labelsize=8)    # fontsize of the x and y labels
plt.rc('xtick', labelsize=8)    # fontsize of the tick labels
plt.rc('ytick', labelsize=8)    # fontsize of the tick labels
plt.rc('legend', fontsize=8)    # legend fontsize
plt.rc('figure', titlesize=8)  # fontsize of the figure title

(n_row, n_col) = (3, 5)
gs = GridSpec(n_row, n_col)

ax = fig.add_subplot(gs[0, 0])
plot2D(x1, x2, z, torch.min(z), torch.max(z), 'Exact')
ax = fig.add_subplot(gs[1, 0])
plot2D(x1, x2, u, torch.min(z), torch.max(z), 'HT+NN')
ax = fig.add_subplot(gs[2, 0])
plot2D(x1, x2, z-u, t='Error')

ax = fig.add_subplot(gs[0, 1])
plot2D(x1, x2, zpx1, torch.min(zpx1), torch.max(zpx1), 'ux_Exact')
ax = fig.add_subplot(gs[1, 1])
#plot2D(x1, x2, upx1, t= 'ux_HT+NN')
plot2D(x1, x2, upx1,torch.min(zpx1), torch.max(zpx1), t= 'ux_HT+NN')
ax = fig.add_subplot(gs[2, 1])
plot2D(x1, x2, zpx1-upx1, t='Error')

ax = fig.add_subplot(gs[0, 2])
plot2D(x1, x2, zpx1x1, torch.min(zpx1x1), torch.max(zpx1x1), 'uxx_Exact')
ax = fig.add_subplot(gs[1, 2])
#plot2D(x1, x2, upx1x1 , t= 'uxx_HT+NN')
plot2D(x1, x2, upx1x1 , torch.min(zpx1x1), torch.max(zpx1x1), t= 'uxx_HT+NN')
ax = fig.add_subplot(gs[2, 2])
plot2D(x1, x2, zpx1x1-upx1x1, t='Error')

ax = fig.add_subplot(gs[0, 3])
plot2D(x1, x2, zpx2, torch.min(zpx2), torch.max(zpx2), 'uy_Exact')
ax = fig.add_subplot(gs[1, 3])
#plot2D(x1, x2, upx2,t= 'uy_HT+NN')
plot2D(x1, x2, upx2, torch.min(zpx2), torch.max(zpx2) ,t= 'uy_HT+NN')
ax = fig.add_subplot(gs[2, 3])
plot2D(x1, x2, zpx2-upx2, t='Error')

ax = fig.add_subplot(gs[0, 4])
plot2D(x1, x2, zpx2x2, torch.min(zpx2x2), torch.max(zpx2x2), 'uyy_Exact')
ax = fig.add_subplot(gs[1, 4])
#plot2D(x1, x2, upx2x2 ,t= 'uyy_HT+NN')
plot2D(x1, x2, upx2x2 ,torch.min(zpx2x2), torch.max(zpx2x2),t= 'uyy_HT+NN')
ax = fig.add_subplot(gs[2, 4])
plot2D(x1, x2, zpx2x2-upx2x2, t= 'Error')

plt.show()
#print(pinn.D)
fig.savefig('./out1.png')