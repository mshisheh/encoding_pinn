
import matplotlib.pyplot as plt

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as Fun
import torch.utils.dlpack
import time
import os
from network import Base
from data_generate import generate_Customized_2d_train_data, generate_Customized_2d_test_data
from plot import Customized_2d_plot


device = torch.device("cuda:0")

seed = 500

dtype = torch.float32
dtype_long = torch.cuda.LongTensor
np.random.seed(seed) 
torch.manual_seed(seed)
torch.cuda.manual_seed_all(seed)
torch.backends.cudnn.benchmark = False
os.environ["PYTHONHASHSEED"] = str(seed)
torch.backends.cudnn.deterministic = True
torch.set_default_dtype(torch.float32)
os.environ['CUDA_LAUNCH_BLOCKING'] = "1"


# Parameters
x_min = -1.0
x_max = 1.0
y_min = 0.0
y_max = 2.0

N_f = 200000  # number of collocations
N_bc = 200000  # number of bcs
N_d = 200000 # labeled data
omega = 1.0



#######################################
nodes_per_layer = 16
#######################################

class PINN(nn.Module):
    def __init__(self):
        super(PINN, self).__init__()

        #initializing feature maps
        self.n_features = 4
        self.network = Base().to(device)
        self.set_optim('lbfgs')

        
    def forward(self, x, y):

        x = (x - x_min)/(x_max - x_min)
        y = (y - y_min)/(y_max - y_min)
        x = x*2-1
        y = y*2-1
        x = torch.cat([y, x], dim=-1).unsqueeze(0).unsqueeze(0)
        out = self.network(x)
        
        return out

    def loss_pde(self, x, y, uc):

        u = self.forward(x,y)

        u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
        u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]
        u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
        u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]
    

        f =  u_yy + 2.0 * u_xx -4.0 + omega**2.0 * (x**2 + 2 * y**2) * torch.sin(omega * x * y)
        #f =  u_yy + u_xx -4.0 + omega**2.0 * (x**2.0 + y**2.0) * torch.sin(omega * x * y)
        mse_pde = torch.mean(f ** 2.0)

        return mse_pde
    
    def loss_bc(self, x, y, u_bc):
        u = self.forward(x, y)
        mse_bc = torch.mean((u - u_bc)**2.0)
        return mse_bc

    def derivitives(self, x, y):

        u = self.forward(x, y)
        #u = self.decoder(F)

        u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
        u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
        u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]
        u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]
        
        return u_x, u_y, u_xx, u_yy

    def set_optim(self, optim_type):
        if optim_type == 'lbfgs':
            lr = 1.0
            self.optim = torch.optim.LBFGS(
            self.network.parameters(), 
            lr=lr, 
            #max_iter=self.max_iter, 
            #max_eval=50000,
            #history_size=50,
            #tolerance_grad=1e-6,
            #tolerance_change=1.0 * np.finfo(float).eps,
            line_search_fn="strong_wolfe"       # can be "strong_wolfe"
            )
        else:
            lr = 1e-2
            self.optim = torch.optim.Adam(self.parameters(), lr= lr , weight_decay=1e-5)

    def closure(self):
        pinn.optim.zero_grad()

        train_data = int(N_f/2.0)
        bc_data = int(N_bc/2.0)
        self.pde_loss = self.loss_pde(self.xc[:train_data], self.yc[:train_data], self.uc[:train_data])
        self.bc_loss = self.loss_bc(self.xb, self.yb, self.ub)
        loss =  1e-5 * self.pde_loss + self.bc_loss 
        loss.backward()

        pde_loss = self.loss_pde(self.yc[train_data:], self.xc[train_data:], self.uc[train_data:])
        bc_loss = self.loss_bc(self.yb[bc_data:], self.xb[bc_data:], self.ub[bc_data:])

        self.val_loss = 1e-5 * pde_loss + bc_loss
        self.train_loss = loss

        return loss
    
    def train(self):
        for iter in range(num_epochs):
            self.network.train()
            self.yc, self.xc, self.uc, self.yb, self.xb, self.ub = generate_Customized_2d_train_data(N_f, N_bc, omega = omega, domain_size=[x_min, x_max, y_min, y_max])
            loss = self.optim.step(self.closure)

            if iter % 15 ==0:
                print('Iter %d, Loss: %.5e, Loss_u: %.5e, Loss_b: %.5e, Loss_f: %.5e, val_loss: %.5e'%(
                    iter+1, self.bc_loss + self.pde_loss, self.bc_loss, 0.0, self.val_loss, 0.0))

            losses['train'].append(self.train_loss.detach().cpu().item())
            losses['val'].append(self.val_loss.detach().cpu().item())
            #losses['bc'].append(bc_loss.detach().cpu().item())
            #losses['pde'].append(pde_loss.detach().cpu().item())

            losses['total'].append(loss.detach().cpu().item())

            self.test(iter)
            
            if iter > 1  and iter % 50 == 0:
                if losses['val'][-1] <= min(losses['val']):
                    torch.save(pinn.state_dict(), "./best_model_data_"+str(int(omega))+"_2d.pt")


    def test(self, it):
        y_test, x_test, u_test = generate_Customized_2d_test_data(250, omega = omega, domain_size=[x_min, x_max, y_min, y_max])
        self.network.eval()
        u_pred = self.forward(x_test, y_test)
        
        u_pred_arr = u_pred.detach().cpu().numpy()
        u_test_arr = u_test.detach().cpu().numpy()

        l2_loss = np.linalg.norm(u_pred_arr - u_test_arr) / np.linalg.norm(u_test_arr)
        if it % 15 ==0 :
            print('[Test Iter:%d, Loss: %.5e]'%(it, l2_loss))
        if it % 25 == 0 :
            Customized_2d_plot(it, y_test, x_test, u_pred.detach(), u_test, 250, './figures/original', 'pde')

if __name__ == "__main__":


    num_epochs = 100 
    losses = {"train": [], "val": [] , "bc" : [],"pde": [],"total":[]}
    pinn = PINN()
    
    #scheduler = torch.optim.lr_scheduler.MultiStepLR(pinn.optim, milestones= np.linspace(0,num_epochs,drop).tolist(), gamma=0.5)
    s = time.time()
    
    pinn.train()

    torch.save(pinn.state_dict(), "./last_model_data_" + str(int(omega))+"_2d.pt")

    fig, ax = plt.subplots(1, 3 , figsize=(18.0, 4.5), constrained_layout = True)

    ax[0].semilogy(losses["train"] , label='Training data')
    ax[0].semilogy(losses["val"] , label='Validation data')
    ax[0].set(xlabel='Epoch', ylabel='mse')
    ax[0].legend()

    
    ax[1].semilogy(losses["bc"] , label='pde')
    #ax1.semilogy(losses["val"] , label='Validation data')
    ax[1].set(xlabel='Epoch', ylabel='mse')
    ax[1].legend()

    ax[2].semilogy(losses["total"] , label='total')
    #ax1.semilogy(losses["val"] , label='Validation data')
    ax[2].set(xlabel='Epoch', ylabel='mse')
    ax[2].legend()

    fig.savefig('loss2.png' , dpi = 300)




