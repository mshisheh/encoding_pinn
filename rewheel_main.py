
import matplotlib.pyplot as plt

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as Fun
import torch.utils.dlpack
import time
from torch.autograd import grad
from scipy.stats import qmc
from interpolators import bicubic_interp , bispline_interp , bilinear_interp , expon_interp

from grid_sample import grid_sample_2d
from cosine_sampler_2d import CosineSampler2d


Sobol2d = qmc.Sobol(d=2, scramble=True)
Sobol1d = qmc.Sobol(d=1, scramble=True)

res = [9 , 14  , 23, 32 , 55 , 94]

#dtype = torch.cuda.FloatTensor
dtype = torch.float32
dtype_long = torch.cuda.LongTensor
torch.backends.cuda.matmul.allow_tf32 = True
torch.set_default_dtype(torch.float32)

# Parameters
x_min = 0.0
x_max = 1.0
y_min = 0.0
y_max = 1.0

N_f = 20000  # number of collocations
N_bc = 1024  # number of bcs
N_d = 20000 # labeled data
omega = 1
u = lambda w, x, y: np.sin(w*x*y) + x**2 + 2*x*y
uL = lambda w, y: torch.sin(w*x_min*y) + x_min**2 + 2*x_min*y
uR = lambda w, y: torch.sin(w*x_max*y) + x_max**2 + 2*x_max*y
uB = lambda w, x: torch.sin(w*x*y_min) + x**2 + 2*x*y_min
uT = lambda w, x: torch.sin(w*x*y_max) + x**2 + 2*x*y_max


device = torch.device("cuda:0")

B = torch.tensor([[-1,1,-1,1],[0,0,0,1],[1,1,1,1],[8,4,2,1]] , device = device).type(dtype)
C = torch.tensor([[1, 0, 0 , 0], [0 , 0 ,1, 0] , [-3, 3, -2 , -1] , [2, -2, 1, 1]] , device = device).type(torch.float32)
B_inv = torch.inverse(B).to(device)





nodes_per_layer = 64



########


ub = np.array([x_max])
lb = np.array([x_min])



class PINN(nn.Module):
    def __init__(self):
        super(PINN, self).__init__()

        #initializing feature maps
        self.n_features = 2
        pad = 3 #change this to 3 for bispline and bicubic and 1 for anything else
        
        self.alpha0 = nn.Parameter(torch.ones(size=(res[0]+pad,res[0]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)
        self.alpha1 = nn.Parameter(torch.ones(size=(res[1]+pad,res[1]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)
        self.alpha2 = nn.Parameter(torch.ones(size=(res[2]+pad,res[2]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)
        self.alpha3 = nn.Parameter(torch.ones(size=(res[3]+pad,res[3]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)
        self.alpha4 = nn.Parameter(torch.ones(size=(res[4]+pad,res[4]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)



        self.F_active = [self.alpha0 , self.alpha1 , self.alpha2 , self.alpha3]

       
        self.decoder = nn.Sequential(
            nn.LazyLinear(64, bias=True, device=device),
            #nn.Linear(2,64, bias=True, device=device),
            nn.Tanh(),
            nn.Linear(64,64, bias=True, device=device),
            nn.Tanh(),
            nn.Linear(64,1, bias=True, device=device),
            #nn.Tanh()
        )



        class Identity(torch.autograd.Function):
            @staticmethod
            def forward(ctx, x):
                ctx.save_for_backward(x)
                return x

            @staticmethod
            def backward(ctx, grad_out):
                x, = ctx.saved_tensors
                return IBackward.apply(grad_out , x)

        class IBackward(torch.autograd.Function):
            @staticmethod
            def forward(ctx, grad_out , x):
                ctx.save_for_backward(x)
                return grad_out

            @staticmethod
            def backward(ctx, grad_out):

                x,  = ctx.saved_tensors

                
                f = self.encoder(x).clone().detach() 
                u = self.decoder(f).clone().detach()  #(N , )


                eps = 1e-3
                xi = x[:,0]
                xj = x[:,1]

                x_im = torch.stack([xi-eps , xj] , dim = 1)
                x_ip = torch.stack([xi+eps , xj], dim = 1)

                
                f_im , f_ip = self.encoder(x_im).clone().detach() , self.encoder(x_ip).clone().detach()
                u_im , u_ip = self.decoder(f_im).clone().detach() , self.decoder(f_ip).clone().detach()

                x_imm = torch.stack([xi-2*eps , xj] , dim = 1)
                x_ipp = torch.stack([xi+2*eps , xj], dim = 1)

                f_imm , f_ipp = self.encoder(x_imm).clone().detach() , self.encoder(x_ipp).clone().detach()
                u_imm , u_ipp = self.decoder(f_imm).clone().detach() , self.decoder(f_ipp).clone().detach()

                if order ==2 :
                    uxx = (u_ip + u_im -2*u)/ (eps**2)
                elif order ==4 : 
                    uxx = (-u_ipp + 16*u_ip - 30*u + 16*u_im -u_imm)/ (12*eps**2)

                x_jm = torch.stack([xi , xj-eps] , dim = 1)
                x_jp = torch.stack([xi , xj+eps], dim = 1)
                
                f_jm , f_jp = self.encoder(x_jm).clone().detach() , self.encoder(x_jp).clone().detach()
                u_jm , u_jp = self.decoder(f_jm).clone().detach() , self.decoder(f_jp).clone().detach()

                x_jmm = torch.stack([xi , xj-2*eps] , dim = 1)
                x_jpp = torch.stack([xi , xj+2*eps], dim = 1)
                
                f_jmm , f_jpp = self.encoder(x_jmm).clone().detach() , self.encoder(x_jpp).clone().detach()
                u_jmm , u_jpp = self.decoder(f_jmm).clone().detach() , self.decoder(f_jpp).clone().detach()

                if order == 2:
                    uyy = (u_jp + u_jm-2*u)/ (eps**2)
                elif order == 4:
                    uyy = (-u_jpp + 16*u_jp - 30*u + 16*u_jm -u_jmm)/ (12*eps**2)


                DD = torch.cat([uxx , uyy] , dim = 1)  #*grad_out

                return DD  ,DD

        self.custom_grad = Identity.apply

    
        
    def encoder(self, x):


        features  = []
        for idx , alpha in enumerate(self.F_active):

            if interpolator == 'bilinear':

                x_ = res[idx]*x
                X, Y = torch.unsqueeze(x_[:,0], dim=0), torch.unsqueeze(x_[:,1], dim=0)

                fs = []
                for i in range(self.n_features):
                    
                    image = torch.unsqueeze(alpha[:,:,i],2).to(device)

                    f = bilinear_interp(image, X, Y)[:,0]
                    fs.append(f)
                    
                F = torch.stack(fs, dim = 1)
                    
                features.append(F)

            if interpolator == 'bicubic':

                x_ = res[idx]*x + 1
                X , Y = x_[:,0] , x_[:,1]
                
                fs = []
                for i in range(self.n_features):                

                    #Image = torch.ones(res[idx]+3 , res[idx]+3).to(device)
                    #Image[1:res[idx]+2,1:res[idx]+2] =alpha[:,:,i]

                    Image = alpha[:,:,i].to(device)

                    f = bicubic_interp(Image , X , Y)
                    fs.append(f)

                F = torch.stack(fs , dim = 1)
                
                features.append(F)

            if interpolator == 'bispline':

                x_ = res[idx]*x + 1
                X , Y = x_[:,0] , x_[:,1]
                
                fs = []
                for i in range(self.n_features):                

                    #Image = torch.ones(res[idx]+3 , res[idx]+3).to(device)
                    #Image[1:res[idx]+2,1:res[idx]+2] =alpha[:,:,i]

                    Image = alpha[:,:,i].to(device)

                    f = bispline_interp(Image , X , Y)
                    fs.append(f)

                F = torch.stack(fs , dim = 1)
                
                features.append(F)

            if interpolator == 'expon':

                x_ = res[idx]*x 
                X , Y = x_[:,0] , x_[:,1]
                
                fs = []
                for i in range(self.n_features):                

                    Image = alpha[:,:,i].to(device)

                    f = expon_interp(Image , X , Y)
                    fs.append(f)

                F = torch.stack(fs , dim = 1)
                
                features.append(F)

      
            if encoder_mode == 'concat':
                F = torch.cat(tuple(features) , 1)

            elif encoder_mode == 'add':
                F = torch.zeros_like(features[0])
                for f in features:
                    F = F + f
        

        return F



        
    def forward(self, x):

        if enable_encoder:
            
            #x = self.custom_grad(x)
        
            x = self.encoder(x)

        x = self.decoder(x)
        

        return x

    def loss_pde(self, xyt):
        #data[:,:-1]
        xyt = xyt[:,:-1].clone()
        xyt.requires_grad = True

        u = self.forward(xyt)

        u_xyt = grad(u.sum(), xyt, create_graph=True)[0]
        u_xx = grad(u_xyt[:,0].sum(), xyt, create_graph=True, allow_unused=True)[0][:,0]
        u_yy = grad(u_xyt[:,1].sum(), xyt, create_graph=True, allow_unused=True)[0][:,1]
        

        pde = u_yy + 2 * u_xx + omega **2.0 * (xyt[:,0]**2.0 + 2 * xyt[:,1]**2.0) * torch.sin(omega * xyt[:,0] * xyt[:,1]) - 4.0

        mse_pde = torch.mean(torch.square(pde))
        return mse_pde

    def derivitives(self, xyt):
        xyt = xyt.clone()
        xyt.requires_grad = True

        #F = self.encoder(xyt)

        u = self.forward(xyt)
        #u = self.decoder(F)

        u_xyt = grad(u.sum(), xyt, create_graph=True)[0]

        u_xx = grad(u_xyt[:,0].sum(),xyt, create_graph=True, allow_unused=True)[0][:,0]
        u_yy = grad(u_xyt[:,1].sum(), xyt, create_graph=True, allow_unused=True)[0][:,1]
        
        return u_xyt, u_xx, u_yy



def generate_label_data(DATA , N_d, i ,split_ratio = 0.5):

    DATA = DATA[np.random.permutation(len(DATA)), :]
    data = DATA[:N_d, :]
    split = int(split_ratio * len(data))
    train_data = data[:split,:]
    val_data = data[split:,:]

    return train_data , val_data

def bc_generate(N_bc, split_ratio = 0.5 ,Sobol1d = Sobol1d):
    x_pointsB = Sobol1d.random(N_bc)
    y_pointsB = x_pointsB*0.0
    bcB = np.hstack((x_pointsB, y_pointsB))
    ubcB = u(omega, x_pointsB, y_pointsB)

    x_pointsT = Sobol1d.random(N_bc)
    y_pointsT = x_pointsT*0.0 + 1.0
    bcT = np.hstack((x_pointsT, y_pointsT))
    ubcT = u(omega, x_pointsT, y_pointsT)

    y_pointsL = Sobol1d.random(N_bc)
    x_pointsL = y_pointsL*0.0
    bcL = np.hstack((x_pointsL, y_pointsL))
    ubcL = u(omega, x_pointsL, y_pointsL)

    y_pointsR = Sobol1d.random(N_bc)
    x_pointsR = y_pointsR*0.0 + 1.0
    bcR = np.hstack((x_pointsR, y_pointsR))
    ubcR = u(omega, x_pointsR, y_pointsR)

    bc = np.vstack((bcB, bcT, bcL, bcR))
    u_bc = np.vstack((ubcB, ubcT, ubcL, ubcR))

    x_bc = torch.tensor(bc, dtype=torch.float32).to(device)
    u_bc = torch.tensor(u_bc, dtype=torch.float32).to(device)

    split = int(split_ratio * len(x_bc))
    train_x_bc = x_bc[:split,:]
    val_x_bc= x_bc[split:,:]
    train_u_bc= u_bc[:split,:]
    val_u_bc= u_bc[split:,:]

    return train_x_bc , train_u_bc
    
pinn = PINN()

def loss_data(data):
    u = pinn(data[:,:-1])
    mse_data = torch.mean(torch.square(u - data[:,-1].reshape(-1,1)))
    return mse_data

def loss_bc( xyt, u_bc):
    xyt = xyt.clone()
    xyt.requires_grad = True
    u = pinn(xyt)
    mse_bc = torch.mean(torch.square(u - u_bc))
    return mse_bc


num_epochs = 2500 
lr = 1e-3
drop = 5 # how many times drop the lr
optim = torch.optim.Adam(pinn.parameters(), lr= lr , weight_decay=1e-5)
#scheduler =torch.optim.lr_scheduler.ReduceLROnPlateau(optim, factor=0.75, patience=50, threshold=0.0001, min_lr=1e-6 , verbose=True)
scheduler = torch.optim.lr_scheduler.MultiStepLR(optim, milestones= np.linspace(0,num_epochs,drop).tolist(), gamma=0.75)
losses = {"train": [], "val": [] , "bc" : [],"pde": [],"total":[]}
gamma = 1


enable_encoder = True
interpolator = 'bilinear' #'bicubic'  or 'bilinear' or 'bispline' , or 'expon'
encoder_mode = 'concat'   #'concat'  or 'add'
order = 4

if __name__ == "__main__":

    DATA = torch.tensor(np.load(f'./real_values_{omega}.npy') , dtype = dtype).to(device)

    s = time.time()
    for iter in range(num_epochs):
        train_data , val_data = generate_label_data(DATA = DATA , N_d=len(DATA), i=iter ,split_ratio = 0.5)
        x_bc , u_bc =  bc_generate(N_bc, split_ratio = 0.5 ,Sobol1d = Sobol1d)
        optim.zero_grad()
        train_loss = loss_data(train_data)
        val_loss = loss_data(val_data)
        bc_loss = loss_bc(x_bc , u_bc)
        pde_loss = 1#pinn.loss_pde(train_data)

        loss =  train_loss + bc_loss # gamma * pde_loss + train_loss #

        losses['train'].append(train_loss.detach().cpu().item())
        losses['val'].append(val_loss.detach().cpu().item())
        losses['bc'].append(bc_loss.detach().cpu().item())
        #losses['pde'].append(pde_loss.detach().cpu().item())

        losses['total'].append(loss.detach().cpu().item())

        loss.backward()
        optim.step()

        if iter %100 ==0:
            print(f'Epoch: {iter}')
            print(f"Train Loss: ... {train_loss.item():.5e}")
            print(f"Val Loss: ... {val_loss.item():.5e}")
            #print(f"PDE Loss: ... {pde_loss.item():.5e}")
            temp = "{:.2e}".format((time.time() - s)/60)
            print(f'Elapsed time: ... ' + temp + ' ... min')
            print('===========================================')
        if iter+1 == num_epochs:
            print(f'Epoch: {num_epochs}')
            print(f"Train Loss: ... {train_loss.item():.5e}")
            print(f"Val Loss: ... {val_loss.item():.5e}")
            #print(f"PDE Loss: ... {pde_loss.item():.5e}")
            temp = str((time.time() - s)/60)
            print(f'Elapsed time: ... ' + temp + ' ... min')
            print('===========================================')

        if iter > 1  and iter % 50 == 0:
            if losses['val'][-1] <= min(losses['val']):
                torch.save(pinn.state_dict(), "./best_model_data_"+str(int(omega))+"_2d.pt")



    torch.save(pinn.state_dict(), "./last_model_data_" + str(int(omega))+"_2d.pt")

    fig, ax = plt.subplots(1, 3 , figsize=(18.0, 4.5), constrained_layout = True)

    ax[0].semilogy(losses["train"] , label='Training data')
    ax[0].semilogy(losses["val"] , label='Validation data')
    ax[0].set(xlabel='Epoch', ylabel='mse')
    ax[0].legend()

    
    ax[1].semilogy(losses["bc"] , label='pde')
    #ax1.semilogy(losses["val"] , label='Validation data')
    ax[1].set(xlabel='Epoch', ylabel='mse')
    ax[1].legend()

    ax[2].semilogy(losses["total"] , label='total')
    #ax1.semilogy(losses["val"] , label='Validation data')
    ax[2].set(xlabel='Epoch', ylabel='mse')
    ax[2].legend()

    fig.savefig('loss2.png' , dpi = 300)




