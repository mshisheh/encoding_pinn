################### ########################

import numpy as np
import pandas as pd
import os
import torch
import argparse

from network import DNN

from mesh_pinn import *

parser = argparse.ArgumentParser(description='Main file for solving PDE', add_help=False)

# Training
parser.add_argument('--seed', type=float, default=1234, help='Set the seed number')

# Optimization
parser.add_argument('--lr', type = float, default=1.0, help='learning rate (default: 1.0')
parser.add_argument('--optim', type=str, default='lbfgs', help='Type of optimization (lbfgs)')

# PDE 
parser.add_argument('--pde', type = str, default='customized_2d', help = 'Name of the pde')
parser.add_argument('--domain-size', type=float, nargs='+', default = [0.0, 1.0, 0.0, 1.0], help = '[xmin, xmax, ymin, ymax]')
parser.add_argument('--omega', type = float, default=1.0)

# Network



if __name__ == "__main__":

    args = parser.parse_args()
    np.random.seed(args.seed)
    torch.manual_seed(args.seed)
    torch.cuda.manual_seed_all(args.seed)
    os.environ["PYTHONHASHSEED"] = str(args.seed)

    if torch.cuda.is_available():
        device = torch.device('cuda')
        torch.set_default_tensor_type('torch.cuda.FloatTensor')
    else:
        device = torch.device('cpu')
    

    model = Mesh_PINN(args)



