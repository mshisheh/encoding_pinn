
import torch
from utils.grid_sample import *

from cosine_sampler_2d import CosineSampler2d

class Mesh_encoding(torch.nn.modules):
    def __init__(self, args) -> None:
        self.args = args
        mesh, feature = args.mesh, args.F
        self.cells = torch.nn.Parameter(torch.rand(mesh, feature, mesh, mesh))
        self.cells.data.uniform_(-1e-5,1e-5)
        self.cells.requires_grad = True

    def forward(self, x):
        x = x.repeat([self.cells.shape[0],1,1,1])
        ''' 1) see the Section "Mesh-agnostic representations through interpolation" in the paper. 
                "grid_sample_2d" function is an interpolation function
            2) also, see the Section "Multigrid representations". in the paper.
                we can shift the input coordinates by using "offset=True". '''
        
        if self.args.cuda_off:
            feats = grid_sample_2d(self.cells, x, step=self.args.interp, offset=True)
        else:
            feats = CosineSampler2d.apply(self.cells, x, 'zeros', True, 'cosine', True)
        

        return feats.sum(0).view(self.args.F,-1).t()