
import matplotlib.pyplot as plt

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as Fun
import torch.utils.dlpack
import time
from torch.autograd import grad
from scipy.stats import qmc
from interpolators import bicubic_interp , bispline_interp , bilinear_interp , expon_interp

from grid_sample import grid_sample_2d
from cosine_sampler_2d import CosineSampler2d


Sobol2d = qmc.Sobol(d=2, scramble=True)
Sobol1d = qmc.Sobol(d=1, scramble=True)

res = [16] #[9 , 14  , 23, 32 , 55 , 94]

#dtype = torch.cuda.FloatTensor
dtype = torch.float32
dtype_long = torch.cuda.LongTensor
torch.backends.cuda.matmul.allow_tf32 = True
torch.set_default_dtype(torch.float32)

# Parameters
x_min = 0.0
x_max = 1.0
y_min = 0.0
y_max = 1.0

N_f = 20000  # number of collocations
N_bc = 1024  # number of bcs
N_d = 20000 # labeled data
omega = 1
u = lambda w, x, y: np.sin(w*x*y) + x**2 + 2*x*y
uL = lambda w, y: torch.sin(w*x_min*y) + x_min**2 + 2*x_min*y
uR = lambda w, y: torch.sin(w*x_max*y) + x_max**2 + 2*x_max*y
uB = lambda w, x: torch.sin(w*x*y_min) + x**2 + 2*x*y_min
uT = lambda w, x: torch.sin(w*x*y_max) + x**2 + 2*x*y_max


device = torch.device("cuda:0")

# B = torch.tensor([[-1,1,-1,1],[0,0,0,1],[1,1,1,1],[8,4,2,1]] , device = device).type(dtype)
# C = torch.tensor([[1, 0, 0 , 0], [0 , 0 ,1, 0] , [-3, 3, -2 , -1] , [2, -2, 1, 1]] , device = device).type(torch.float32)
# B_inv = torch.inverse(B).to(device)


#######################################
nodes_per_layer = 16
#######################################

ub = np.array([x_max])
lb = np.array([x_min])

class PINN(nn.Module):
    def __init__(self):
        super(PINN, self).__init__()

        #initializing feature maps
        self.n_features = 4
        pad = 0 #change this to 3 for bispline and bicubic and 1 for anything else
        
        self.alpha0 = nn.Parameter(torch.rand(size=(1, self.n_features, res[0]+pad,res[0]+pad,) , dtype=dtype , device = device) ,requires_grad=True)
        # self.alpha1 = nn.Parameter(torch.ones(size=(res[1]+pad,res[1]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)
        # self.alpha2 = nn.Parameter(torch.ones(size=(res[2]+pad,res[2]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)
        # self.alpha3 = nn.Parameter(torch.ones(size=(res[3]+pad,res[3]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)
        # self.alpha4 = nn.Parameter(torch.ones(size=(res[4]+pad,res[4]+pad,self.n_features) , dtype=dtype , device = device) ,requires_grad=True)

        self.alpha0.data.uniform_(-1e-5,1e-5)
        self.F_active = [self.alpha0]
       
        self.decoder = nn.Sequential(
            nn.Linear(self.n_features, nodes_per_layer, bias=True, device=device),
            #nn.Linear(2,nodes_per_layer, bias=True, device=device),
            nn.Tanh(),
            nn.Linear(nodes_per_layer,1, bias=True, device=device),
            #nn.Tanh()
        )


    def encoder(self, x):

        features  = []
        x = 2 * x -1
        for idx , alpha in enumerate(self.F_active):
            
            x = x.repeat([alpha.shape[0],1,1,1])

            F = grid_sample_2d(alpha, x, step='bilinear', offset=True)
            #f1 = grid_sample_2d(alpha[...,1].unsqueeze(0).unsqueeze(0), x.unsqueeze(0).unsqueeze(0), step='bilinear', offset=True)

            #F = torch.hstack((f0.reshape(-1,1), f1.reshape(-1,1)))
            
            features.append(F.sum(0).view(self.n_features,-1).t())
            
        F = torch.cat(tuple(features) , 1)

        return F

        
    def forward(self, x, y):

        x = torch.concat([x.reshape(-1,1),y.reshape(-1,1)], axis = -1)
        if enable_encoder:
            
            #x = self.custom_grad(x)
        
            x = self.encoder(x)

        x = self.decoder(x)
        

        return x

    def loss_pde(self, xyt):
        #data[:,:-1]
        xyt = xyt[:,:-1].clone()
        xyt.requires_grad = True
        x = xyt[:,0]
        y = xyt[:,1]

        u = self.forward(x,y)

        u_x_y = torch.autograd.grad(u, xyt, torch.ones_like(u), True, True)[0]
        u_y = u_x_y[:,1]
        u_x = u_x_y[:,0]
        u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]
        #u_x = torch.autograd.grad(u, xyt[:,0], torch.ones_like(u), True, True)[0]
        u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]


        # u_xyt = grad(u.sum(), xyt, create_graph=True)[0]
        # u_xx = grad(u_xyt[:,0].sum(), xyt, create_graph=True)[0][:,0]
        # u_yy = grad(u_xyt[:,1].sum(), xyt, create_graph=True)[0][:,1]
        

        pde = u_yy + 2 * u_xx + omega **2.0 * (xyt[:,0]**2.0 + 2 * xyt[:,1]**2.0) * torch.sin(omega * xyt[:,0] * xyt[:,1]) - 4.0

        mse_pde = torch.mean(torch.square(pde))
        return mse_pde

    def derivitives(self, xyt):
        xyt = xyt.clone()
        xyt.requires_grad = True

        #F = self.encoder(xyt)
        x = xyt[:,0]
        y = xyt[:,1]
        u = self.forward(x, y)
        #u = self.decoder(F)

        u_x_y = torch.autograd.grad(u, xyt, torch.ones_like(u), True, True)[0]
        u_y = u_x_y[:,1]
        u_x = u_x_y[:,0]
        u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]
        #u_x = torch.autograd.grad(u, xyt[:,0], torch.ones_like(u), True, True)[0]
        u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]


        # u_xyt = grad(u.sum(), xyt, create_graph=True)[0]

        # u_xx = grad(u_xyt[:,0].sum(),xyt, create_graph=True, allow_unused=True)[0][:,0]
        # u_yy = grad(u_xyt[:,1].sum(), xyt, create_graph=True, allow_unused=True)[0][:,1]
        
        return u_x_y, u_xx, u_yy



def generate_label_data(DATA , N_d, i ,split_ratio = 0.5):

    DATA = DATA[np.random.permutation(len(DATA)), :]
    data = DATA[:N_d, :]
    split = int(split_ratio * len(data))
    train_data = data[:split,:]
    val_data = data[split:,:]

    return train_data , val_data

def bc_generate(N_bc, split_ratio = 0.5 ,Sobol1d = Sobol1d):
    x_pointsB = Sobol1d.random(N_bc)
    y_pointsB = x_pointsB*0.0
    bcB = np.hstack((x_pointsB, y_pointsB))
    ubcB = u(omega, x_pointsB, y_pointsB)

    x_pointsT = Sobol1d.random(N_bc)
    y_pointsT = x_pointsT*0.0 + 1.0
    bcT = np.hstack((x_pointsT, y_pointsT))
    ubcT = u(omega, x_pointsT, y_pointsT)

    y_pointsL = Sobol1d.random(N_bc)
    x_pointsL = y_pointsL*0.0
    bcL = np.hstack((x_pointsL, y_pointsL))
    ubcL = u(omega, x_pointsL, y_pointsL)

    y_pointsR = Sobol1d.random(N_bc)
    x_pointsR = y_pointsR*0.0 + 1.0
    bcR = np.hstack((x_pointsR, y_pointsR))
    ubcR = u(omega, x_pointsR, y_pointsR)

    bc = np.vstack((bcB, bcT, bcL, bcR))
    u_bc = np.vstack((ubcB, ubcT, ubcL, ubcR))

    x_bc = torch.tensor(bc, dtype=torch.float32).to(device)
    u_bc = torch.tensor(u_bc, dtype=torch.float32).to(device)

    split = int(split_ratio * len(x_bc))
    train_x_bc = x_bc[:split,:]
    val_x_bc= x_bc[split:,:]
    train_u_bc= u_bc[:split,:]
    val_u_bc= u_bc[split:,:]

    return train_x_bc , train_u_bc
    
pinn = PINN()

def loss_data(data):
    u = pinn(data[:,0], data[:,1])
    mse_data = torch.mean(torch.square(u - data[:,-1].reshape(-1,1)))
    return mse_data

def loss_bc( xyt, u_bc):
    xyt = xyt.clone()
    xyt.requires_grad = True
    u = pinn(xyt[:,0], xyt[:,1])
    mse_bc = torch.mean(torch.square(u - u_bc))
    return mse_bc


num_epochs = 500 
optim_type = 'lbfgs'
drop = 5 # how many times drop the lr


if optim_type == 'lbfgs':
    lr = 1.0
    
    optim = torch.optim.LBFGS(
    pinn.parameters(), 
    lr=lr, 
    #max_iter=self.max_iter, 
    #max_eval=50000,
    #history_size=50,
    #tolerance_grad=1e-6,
    #tolerance_change=1.0 * np.finfo(float).eps,
    line_search_fn="strong_wolfe"       # can be "strong_wolfe"
    )
else:
    lr = 1e-2
    optim = torch.optim.Adam(pinn.parameters(), lr= lr , weight_decay=1e-5)


#scheduler =torch.optim.lr_scheduler.ReduceLROnPlateau(optim, factor=0.75, patience=50, threshold=0.0001, min_lr=1e-6 , verbose=True)
scheduler = torch.optim.lr_scheduler.MultiStepLR(optim, milestones= np.linspace(0,num_epochs,drop).tolist(), gamma=0.5)

losses = {"train": [], "val": [] , "bc" : [],"pde": [],"total":[]}
gamma = 1


enable_encoder = True
interpolator = 'bilinear' #'bicubic'  or 'bilinear' or 'bispline' , or 'expon'
encoder_mode = 'concat'   #'concat'  or 'add'
order = 4


if __name__ == "__main__":

    def closure():
        optim.zero_grad()
        train_loss = loss_data(train_data)
        val_loss = 0.0 #loss_data(val_data)
        pde_loss = pinn.loss_pde(train_data)
        bc_loss = loss_bc(x_bc , u_bc)

        loss =  0.0001 * pde_loss + bc_loss # gamma * pde_loss + train_loss #
        #loss = bc_loss + train_loss
        loss.backward()

        # if iter %100 ==0:
        #     print(f'Epoch: {iter}')
        #     print(f"pde loss: ... {train_loss.item():.5e}")
        #     print(f"bc Loss: ... {bc_loss.item():.5e}")
        #     #print(f"PDE Loss: ... {pde_loss.item():.5e}")
        #     temp = "{:.2e}".format((time.time() - s)/60)
        #     print(f'Elapsed time: ... ' + temp + ' ... min')
        #     print('===========================================')
        # if iter+1 == num_epochs:
        #     print(f'Epoch: {num_epochs}')
        #     print(f"pde Loss: ... {train_loss.item():.5e}")
        #     print(f"bc Loss: ... {bc_loss.item():.5e}")
        #     #print(f"PDE Loss: ... {pde_loss.item():.5e}")
        #     temp = str((time.time() - s)/60)
        #     print(f'Elapsed time: ... ' + temp + ' ... min')
        #     print('===========================================')

        # if iter > 1  and iter % 50 == 0:
        #     if losses['val'][-1] <= min(losses['val']):
        #         torch.save(pinn.state_dict(), "./best_model_data_"+str(int(omega))+"_2d.pt")


        return loss

    DATA = torch.tensor(np.load(f'./real_values_{omega}.npy') , dtype = dtype).to(device)

    s = time.time()
    for iter in range(num_epochs):
        train_data , val_data = generate_label_data(DATA = DATA , N_d=len(DATA), i=iter ,split_ratio = 0.5)
        x_bc , u_bc =  bc_generate(N_bc, split_ratio = 0.5 ,Sobol1d = Sobol1d)

        if optim_type == 'lbfgs':
            loss = optim.step(closure)
            train_loss = loss
            bc_loss = loss
            val_loss = loss
            #scheduler.step()
        else:
            optim.zero_grad()
            train_loss = loss_data(train_data)
            val_loss = loss_data(val_data)
            bc_loss = loss_bc(x_bc , u_bc)
            pde_loss = pinn.loss_pde(train_data)

            #loss =  train_loss + bc_loss # gamma * pde_loss + train_loss #
            loss = 0.0001 * pde_loss + bc_loss
            loss.backward()
            optim.step()
            scheduler.step()

        if iter %100 ==0:
            print(f'Epoch: {iter}')
            print(f"Train Loss: ... {train_loss.item():.5e}")
            print(f"Val Loss: ... {val_loss.item():.5e}")
            #print(f"PDE Loss: ... {pde_loss.item():.5e}")
            temp = "{:.2e}".format((time.time() - s)/60)
            print(f'Elapsed time: ... ' + temp + ' ... min')
            print('===========================================')
        if iter+1 == num_epochs:
            print(f'Epoch: {num_epochs}')
            print(f"Train Loss: ... {train_loss.item():.5e}")
            print(f"Val Loss: ... {val_loss.item():.5e}")
            #print(f"PDE Loss: ... {pde_loss.item():.5e}")
            temp = str((time.time() - s)/60)
            print(f'Elapsed time: ... ' + temp + ' ... min')
            print('===========================================')

        losses['train'].append(train_loss.detach().cpu().item())
        losses['val'].append(val_loss.detach().cpu().item())
        losses['bc'].append(bc_loss.detach().cpu().item())
        #losses['pde'].append(pde_loss.detach().cpu().item())

        losses['total'].append(loss.detach().cpu().item())

        
        if iter > 1  and iter % 50 == 0:
            if losses['val'][-1] <= min(losses['val']):
                torch.save(pinn.state_dict(), "./best_model_data_"+str(int(omega))+"_2d.pt")


    
    torch.save(pinn.state_dict(), "./last_model_data_" + str(int(omega))+"_2d.pt")

    fig, ax = plt.subplots(1, 3 , figsize=(18.0, 4.5), constrained_layout = True)

    ax[0].semilogy(losses["train"] , label='Training data')
    ax[0].semilogy(losses["val"] , label='Validation data')
    ax[0].set(xlabel='Epoch', ylabel='mse')
    ax[0].legend()

    
    ax[1].semilogy(losses["bc"] , label='pde')
    #ax1.semilogy(losses["val"] , label='Validation data')
    ax[1].set(xlabel='Epoch', ylabel='mse')
    ax[1].legend()

    ax[2].semilogy(losses["total"] , label='total')
    #ax1.semilogy(losses["val"] , label='Validation data')
    ax[2].set(xlabel='Epoch', ylabel='mse')
    ax[2].legend()

    fig.savefig('loss2.png' , dpi = 300)




