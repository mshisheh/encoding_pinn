
import matplotlib.pyplot as plt

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as Fun
import torch.utils.dlpack
import time
from torch.autograd import grad
from scipy.stats import qmc
from interpolators import bicubic_interp , bispline_interp , bilinear_interp , expon_interp

from grid_sample import grid_sample_2d
from cosine_sampler_2d import CosineSampler2d


from data_generate import generate_Customized_2d_train_data, generate_Customized_2d_train_data_sobol

n_cells = 2
res = [8, 16, 32, 64] #[4, 8, 16, 32] #[9 , 14  , 23, 32 , 55 , 94]

dtype = torch.float32
dtype_long = torch.cuda.LongTensor
torch.backends.cuda.matmul.allow_tf32 = True
torch.set_default_dtype(torch.float32)

# Parameters
x_min = -1.0
x_max = 1.0
y_min = 0.0
y_max = 5.0

N_f = 100000  # number of collocations
N_bc = 100024  # number of bcs
N_d = 100000 # labeled data
omega = 1

device = torch.device("cuda:0")

#######################################
nodes_per_layer = 16
#######################################

class PINN(nn.Module):
    def __init__(self):
        super(PINN, self).__init__()

        #initializing feature maps
        self.n_features = 4 
        pad = 0 #change this to 3 for bispline and bicubic and 1 for anything else
        
        self.alpha0 = nn.Parameter(torch.rand(size=(n_cells, self.n_features, res[0]+pad,res[0]+pad,) , dtype=dtype , device = device))
        self.alpha0.data.uniform_(-1e-5,1e-5)
        self.alpha0.requires_grad = True

        self.alpha1 = nn.Parameter(torch.rand(size=(n_cells, self.n_features, res[1]+pad,res[1]+pad,) , dtype=dtype , device = device))
        self.alpha1.data.uniform_(-1e-5,1e-5)
        self.alpha1.requires_grad = True

        self.alpha2 = nn.Parameter(torch.rand(size=(n_cells, self.n_features, res[2]+pad,res[2]+pad,) , dtype=dtype , device = device))
        self.alpha2.data.uniform_(-1e-5,1e-5)
        self.alpha2.requires_grad = True

        self.alpha3 = nn.Parameter(torch.rand(size=(n_cells, self.n_features, res[3]+pad,res[3]+pad,) , dtype=dtype , device = device))
        self.alpha3.data.uniform_(-1e-5,1e-5)
        self.alpha3.requires_grad = True

        # self.alpha4 = nn.Parameter(torch.rand(size=(n_cells, self.n_features, res[4]+pad,res[4]+pad,) , dtype=dtype , device = device))
        # self.alpha4.data.uniform_(-1e-5,1e-5)
        # self.alpha4.requires_grad = True

        self.F_active = [self.alpha0, self.alpha1, self.alpha2, self.alpha3]

        self.decoder = nn.Sequential(
            nn.Linear(self.n_features * len(res), nodes_per_layer, bias=True, device=device),
            #nn.Linear(2,nodes_per_layer, bias=True, device=device),
            nn.Tanh(),
            nn.Linear(nodes_per_layer,1, bias=True, device=device),
            #nn.Tanh()
        )


    def encoder(self, x, y):

        features  = []
        
            
        x = (x - x_min)/(x_max - x_min)
        y = (y - y_min)/(y_max - y_min)
        x = x*2-1
        y = y*2-1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)
        x = x.repeat([n_cells,1,1,1])

        for idx , alpha in enumerate(self.F_active):

            #F = grid_sample_2d(self.alpha0, x, step='bilinear', offset=True)
            F = CosineSampler2d.apply(alpha, x, 'zeros', True, 'cosine', True)
            features.append(F.sum(0).view(self.n_features,-1).t())
        
        F = torch.cat(tuple(features) , 1)
        return F


    def encoder_sum(self, x, y):

        features  = []
        
            
        x = (x - x_min)/(x_max - x_min)
        y = (y - y_min)/(y_max - y_min)
        x = x*2-1
        y = y*2-1

        x = torch.cat([x, y], dim=-1).unsqueeze(0).unsqueeze(0)
        temp = torch.zeros(y.shape[0], self.n_features).to(device)
        temp.requires_grad = True
        x = x.repeat([n_cells,1,1,1])

        for idx , alpha in enumerate(self.F_active):

            #F = grid_sample_2d(self.alpha0, x, step='bilinear', offset=True)
            F = CosineSampler2d.apply(alpha, x, 'zeros', True, 'cosine', True)
            temp = temp + F.sum(0).view(self.n_features,-1).t()
        
        return temp
        
    def forward(self, x, y):

        out = self.encoder(x, y)

        out = self.decoder(out)
        
        return out

    def loss_pde(self, x, y, uc):

        u = self.forward(x,y)

        u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
        u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
        u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]
        u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]

        f = u_yy + 2 * u_xx + omega **2.0 * (x**2.0 + 2 * y**2.0) * torch.sin(omega * x * y) - 4.0

        mse_pde = torch.mean(f ** 2.0)
        return mse_pde

    
    def loss_bc(self, x, y, u_bc):
        u = self.forward(x, y)
        mse_bc = torch.mean(torch.square(u - u_bc))
        return mse_bc

    def derivitives(self, x, y):

        u = self.forward(x, y)
        #u = self.decoder(F)

        u_y = torch.autograd.grad(u, y, torch.ones_like(u), True, True)[0]
        u_x = torch.autograd.grad(u, x, torch.ones_like(u), True, True)[0]
        u_yy = torch.autograd.grad(u_y, y, torch.ones_like(u_y), True, True)[0]
        u_xx = torch.autograd.grad(u_x, x, torch.ones_like(u_x), True, True)[0]

        
        return u_x, u_y, u_xx, u_yy

    def set_optim(self, optim_type):
        if optim_type == 'lbfgs':
            lr = 0.1
            self.optim = torch.optim.LBFGS(
            self.parameters(), 
            lr=lr, 
            #max_iter=self.max_iter, 
            #max_eval=50000,
            #history_size=50,
            #tolerance_grad=1e-6,
            #tolerance_change=1.0 * np.finfo(float).eps,
            line_search_fn="strong_wolfe"       # can be "strong_wolfe"
            )
        else:
            lr = 1e-2
            self.optim = torch.optim.Adam(self.parameters(), lr= lr , weight_decay=1e-5)
            self.scheduler = torch.optim.lr_scheduler.MultiStepLR(pinn.optim, milestones= np.linspace(0,num_epochs,drop).tolist(), gamma=0.5)


if __name__ == "__main__":


    num_epochs = 50
    optim_type = 'lbfgs'
    drop = 5 # how many times drop the lr

    losses = {"train": [], "val": [] , "bc" : [],"pde": [],"total":[]}
    gamma = 1

    pinn = PINN()
    pinn.set_optim(optim_type)
    def closure():
        pinn.optim.zero_grad()

        train_data = int(N_f/2.0)
        bc_data = int(N_bc/2.0)
        pde_loss = pinn.loss_pde(xc[:train_data], yc[:train_data], uc[:train_data])
        bc_loss = pinn.loss_bc(xb, yb, ub)
        loss =  1e-5 * pde_loss + bc_loss 
        loss.backward()
        return loss

    s = time.time()

    #y_test, x_test, u_test = generate_Customized_2d_test_data(250, omega, domain_size=[x_min, x_max, y_min, y_max])
    for iter in range(num_epochs):
        pinn.train()
        yc, xc, uc, yb, xb, ub = generate_Customized_2d_train_data_sobol(N_f, N_bc, omega = omega, domain_size=[x_min, x_max, y_min, y_max])
        if optim_type == 'lbfgs':
            loss = pinn.optim.step(closure)
            train_data = int(N_f/2.0)
            bc_data = int(N_bc/2.0)

            # for val
            pde_loss = pinn.loss_pde(xc[train_data:], yc[train_data:], uc[train_data:])
            bc_loss = pinn.loss_bc(xb, yb, ub)

            val_loss = 1e-5 * pde_loss + bc_loss
            train_loss = loss

        elif optim_type == 'adam':
            pinn.optim.zero_grad()
            bc_loss = pinn.loss_bc(xb , yb, ub)
            pde_loss = pinn.loss_pde(xc, yc, uc)

            train_loss = pde_loss
            val_loss = pde_loss

            loss =  pde_loss + bc_loss
            loss.backward()
            pinn.optim.step()
            pinn.scheduler.step()
        else:
            if iter < 50:
                pinn.set_optim('lbfgs')
                loss = pinn.optim.step(closure)
                train_data = int(N_f/2.0)
                bc_data = int(N_bc/2.0)

                # for val
                pde_loss = pinn.loss_pde(xc[train_data:], yc[train_data:], uc[train_data:])
                bc_loss = pinn.loss_bc(xb, yb, ub)

                val_loss = 1e-5 * pde_loss + bc_loss
                train_loss = loss
            else:
                pinn.set_optim('adam')
                pinn.optim.zero_grad()
                bc_loss = pinn.loss_bc(xb , yb, ub)
                pde_loss = pinn.loss_pde(xc, yc, uc)

                loss =  1e-5 * pde_loss + bc_loss
                train_loss = loss
                val_loss = pde_loss
                loss.backward()
                pinn.optim.step()
                pinn.scheduler.step()


        if iter %15 ==0:
            print(f'Epoch: {iter}')
            print(f"Train Loss: ... {train_loss.item():.5e}")
            print(f"Val Loss: ... {val_loss.item():.5e}")
            #print(f"PDE Loss: ... {pde_loss.item():.5e}")
            temp = "{:.2e}".format((time.time() - s)/60)
            print(f'Elapsed time: ... ' + temp + ' ... min')
            print('===========================================')
        if iter+1 == num_epochs:
            print(f'Epoch: {num_epochs}')
            print(f"Train Loss: ... {train_loss.item():.5e}")
            print(f"Val Loss: ... {val_loss.item():.5e}")
            #print(f"PDE Loss: ... {pde_loss.item():.5e}")
            temp = str((time.time() - s)/60)
            print(f'Elapsed time: ... ' + temp + ' ... min')
            print('===========================================')

        losses['train'].append(train_loss.detach().cpu().item())
        losses['val'].append(val_loss.detach().cpu().item())
        #losses['bc'].append(bc_loss.detach().cpu().item())
        #losses['pde'].append(pde_loss.detach().cpu().item())

        losses['total'].append(loss.detach().cpu().item())

        
        if iter > 1  and iter % 50 == 0:
            if losses['val'][-1] <= min(losses['val']):
                torch.save(pinn.state_dict(), "./best_model_data_"+str(int(omega))+"_2d.pt")


    
    torch.save(pinn.state_dict(), "./last_model_data_" + str(int(omega))+"_2d.pt")

    fig, ax = plt.subplots(1, 3 , figsize=(18.0, 4.5), constrained_layout = True)

    ax[0].semilogy(losses["train"] , label='Training data')
    ax[0].semilogy(losses["val"] , label='Validation data')
    ax[0].set(xlabel='Epoch', ylabel='mse')
    ax[0].legend()

    
    ax[1].semilogy(losses["bc"] , label='pde')
    #ax1.semilogy(losses["val"] , label='Validation data')
    ax[1].set(xlabel='Epoch', ylabel='mse')
    ax[1].legend()

    ax[2].semilogy(losses["total"] , label='total')
    #ax1.semilogy(losses["val"] , label='Validation data')
    ax[2].set(xlabel='Epoch', ylabel='mse')
    ax[2].legend()

    fig.savefig('loss2.png' , dpi = 300)




