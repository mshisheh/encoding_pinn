import torch

dtype = torch.float32
dtype_long = torch.cuda.LongTensor
torch.backends.cuda.matmul.allow_tf32 = True
torch.set_default_dtype(torch.float32)
device = torch.device("cuda:0")

B = torch.tensor([[-1,1,-1,1],[0,0,0,1],[1,1,1,1],[8,4,2,1]] , device = device).type(dtype)
C = torch.tensor([[1, 0, 0 , 0], [0 , 0 ,1, 0] , [-3, 3, -2 , -1] , [2, -2, 1, 1]] , device = device).type(torch.float32)
B_inv = torch.inverse(B).to(device)

def bilinear_interp( im, x, y):

    x0 = torch.floor(x).type(dtype_long)
    x1 = x0 + 1
    
    y0 = torch.floor(y).type(dtype_long)
    y1 = y0 + 1

    x0 = torch.clamp(x0, 0, im.shape[1]-1)
    x1 = torch.clamp(x1, 0, im.shape[1]-1)
    y0 = torch.clamp(y0, 0, im.shape[0]-1)
    y1 = torch.clamp(y1, 0, im.shape[0]-1)
    
    Ia = im[ y0, x0 ][0]
    Ib = im[ y1, x0 ][0]
    Ic = im[ y0, x1 ][0]
    Id = im[ y1, x1 ][0]
    
    wa = ((x1.type(dtype)-x) * (y1.type(dtype)-y))
    wb = ((x1.type(dtype)-x) * (y-y0.type(dtype)))
    wc = ((x-x0.type(dtype)) * (y1.type(dtype)-y))
    wd = ((x-x0.type(dtype)) * (y-y0.type(dtype)))

    I = torch.t((torch.t(Ia)*wa)) + torch.t(torch.t(Ib)*wb) + torch.t(torch.t(Ic)*wc) + torch.t(torch.t(Id)*wd)


    return I

def bicubic_interp(im , x , y):

    x0 = torch.floor(x).type(dtype_long)
    x_1 = x0 - 1
    x1 = x0 + 1
    x2 = x0 + 2
    
    y0 = torch.floor(y).type(dtype_long)
    y_1 = y0 - 1
    y1 = y0 + 1
    y2 = y0 + 2

    x0 = torch.clamp(x0, 0, im.shape[1]-1)
    x_1 = torch.clamp(x_1, 0, im.shape[1]-1)
    x1 = torch.clamp(x1, 0, im.shape[1]-1)
    x2 = torch.clamp(x2, 0, im.shape[1]-1)

    y0 = torch.clamp(y0, 0, im.shape[1]-1)
    y_1 = torch.clamp(y_1, 0, im.shape[1]-1)
    y1 = torch.clamp(y1, 0, im.shape[1]-1)
    y2 = torch.clamp(y2, 0, im.shape[1]-1)

    dx , dy = x - x0.type(dtype) , y-y0.type(dtype)

    f_1_1 = im[x_1, y_1]
    f_10 = im[x_1 , y0]
    f_11 = im[x_1 , y1]
    f_12 = im[x_1 , y2]

    f0_1 = im[x0, y_1]
    f00 = im[x0 , y0]
    f01 = im[x0 , y1]
    f02 = im[x0 , y2]

    f1_1 = im[x1, y_1]
    f10 = im[x1 , y0]
    f11 = im[x1 , y1]
    f12 = im[x1 , y2]

    f2_1 = im[x2, y_1]
    f20 = im[x2 , y0]
    f21 = im[x2 , y1]
    f22 = im[x2 , y2]


    line1 = torch.stack([f_1_1 , f_10 , f_11 , f_12] , 1).to(device)
    line2 = torch.stack([f0_1 , f00 , f01 , f02] , 1).to(device)
    line3 = torch.stack([f1_1 , f10 , f11 , f12] , 1).to(device)
    line4 = torch.stack([f2_1 , f20 , f21 , f22] , 1).to(device)

    
    F = torch.stack([line1 , line2 , line3 , line4] , 1).to(device) #memory issues  (N, 4 , 4)



    A = B_inv @ F @ torch.t(B_inv) # (N, 4 , 4)
    #A = A *1e3
    """
    fxy = torch.zeros_like(dx)

    for i in range(4):
        for j in range(4):
            fxy += A[:,i,j] * dx**(3-i) * dy **(3-j)

    """
    Xs = torch.stack([dx**3 , dx**2 , dx , torch.ones_like(dx)] , 1) #(N , 4)
    Xs = torch.unsqueeze(Xs, dim=1) #(N ,1, 4)
    Ys = torch.stack([dy**3 , dy**2 , dy , torch.ones_like(dy)] , 1) #(N, 4)
    Ys_t = torch.unsqueeze(Ys, dim=2) #(N, 4 , 1)

    l = torch.bmm(torch.bmm(Xs , A), Ys_t)

    fxy = l[:,0,0]

    return fxy

def expon_interp(im , x , y):


    x0 = torch.floor(x).type(dtype_long)
    x1 = x0 + 1
    
    y0 = torch.floor(y).type(dtype_long)
    y1 = y0 + 1

    x0 = torch.clamp(x0, 0, im.shape[1]-1)
    x1 = torch.clamp(x1, 0, im.shape[1]-1)
    y0 = torch.clamp(y0, 0, im.shape[0]-1)
    y1 = torch.clamp(y1, 0, im.shape[0]-1)

    f00 = im[ x0, y0 ]
    f10 = im[ x1, y0 ]
    f01 = im[ x0, y1 ]
    f11 = im[ x1, y1 ]


    dx , dy = x - x0.type(dtype) , y-y0.type(dtype)

    fxy = f00* (1-dx)*(1-dy)\
        + f11* dx * dy\
            + f10* (dx+dy)*(1-dy)\
                + f01* (dx+dy)*(1-dx)  
    """
    
    fxy = f00* torch.tan(torch.pi/4*(1-dx)) * torch.tan(torch.pi/4*(1-dy)) \
        + f10* torch.tan(torch.pi/4 * dx) * torch.cos(dy) \
            + f01*  torch.tan(torch.pi/4*(1-dx)) * torch.tan(torch.pi/4 * dy)  \
                + f11* torch.tan(torch.pi/4 * dx *dy)

     """

    #DX = torch.stack([dx , dy] , dim = 1 ).to(device)

    #I = torch.stack([f00 , f10 , f01 , f11] , dim = 1).to(device)

    """
    fxy = f00* (1-dx)*(1-dy)\
        + f11* dx * dy\
            + f10* (dx+dy)*(1-dy)\
                + f01* (dx+dy)*(1-dx)  

                        

    

    fxy = f00* (1-dx)*(1-dy)* torch.exp(dx + dy) \
        + f11* dx * dy* torch.exp(2- dx -dy) \
            + f10* (dx+dy)*(1-dy)* torch.exp(1- dx +dy)\
                + f01* (dx+dy)*(1-dx)* torch.exp(1+ dx -dy)   
                """     
    """
    fxy = f00* (1-dx)*(1-dy)* torch.cos(dx*dy*torch.pi/2) \
        + f11* dx * dy * torch.sin(dx*dy*torch.pi/2)\
            + f10* (dx+dy)*(1-dy)* torch.sin(dx*torch.pi/2)* torch.cos(dy*torch.pi/2)\
                + f01* (dx+dy)*(1-dx) *torch.cos(dx*torch.pi/2)*torch.sin(dy*torch.pi/2)   

                """
    """
    S = lambda x, y : (3*x**2-2*x**3)*(3*y**2-2*y**3)

    fxy = f00* (1-dx)*(1-dy)* S((1-dx) , (1-dy)) \
        + f11* dx * dy *S(dx , dy)\
            + f10* (dx+dy)*(1-dy) *S(dx , (1-dy))\
                + f01* (dx+dy)*(1-dx) *S((1-dx) , dy)   
    

    h = nn.Linear(2,4 , device = device)(DX)
    h = nn.Linear(4,1 , device = device)(h*I)
    """

    #print(h.size() , I.size)


    return fxy

def bispline_interp(im , x , y):

    x0 = torch.floor(x).type(dtype_long)
    #x0 =  x0 + 1
    x_1 = x0 - 1
    x1 = x0 + 1
    x2 = x0 + 2
    
    y0 = torch.floor(y).type(dtype_long)
    #y0 = y0 + 1
    y_1 = y0 - 1
    y1 = y0 + 1
    y2 = y0 + 2

    x0 = torch.clamp(x0, 0, im.shape[1]-1)
    x_1 = torch.clamp(x_1, 0, im.shape[1]-1)
    x1 = torch.clamp(x1, 0, im.shape[1]-1)
    x2 = torch.clamp(x2, 0, im.shape[1]-1)

    y0 = torch.clamp(y0, 0, im.shape[1]-1)
    y_1 = torch.clamp(y_1, 0, im.shape[1]-1)
    y1 = torch.clamp(y1, 0, im.shape[1]-1)
    y2 = torch.clamp(y2, 0, im.shape[1]-1)

    dx , dy = x - x0.type(dtype) , y-y0.type(dtype)

    f_1_1 = im[x_1, y_1]
    f_10 = im[x_1 , y0]
    f_11 = im[x_1 , y1]
    f_12 = im[x_1 , y2]

    f0_1 = im[x0, y_1]
    f00 = im[x0 , y0]
    f01 = im[x0 , y1]
    f02 = im[x0 , y2]

    f1_1 = im[x1, y_1]
    f10 = im[x1 , y0]
    f11 = im[x1 , y1]
    f12 = im[x1 , y2]

    f2_1 = im[x2, y_1]
    f20 = im[x2 , y0]
    f21 = im[x2 , y1]
    f22 = im[x2 , y2]

    fx00 = (f10 - f_10)/2
    fy00 = (f01 - f0_1)/2

    fx01 = (f11 - f_11)/2
    fy01 = (f02 - f00)/2

    fx10 = (f20 - f00)/2
    fy10 = (f11 - f1_1)/2

    fx11 = (f21 - f01)/2
    fy11 = (f12 - f10)/2

    fxy00 = (f11 - f10 - f01 + f00)/4
    fxy11 = (f22 - f12 - f21 + f11)/4
    fxy01 = (f12 - f11 - f02 + f01)/4
    fxy10 = (f21 - f11 - f20 + f10)/4


    N = x0.shape[0]

    F = torch.zeros([N , 4 , 4]).to(device)
    F[:,0,0] = f00
    F[:,0,1] = f01
    F[:,1,0] = f10
    F[:,1,1] = f11
   

    F[:,0,2] = fy00
    F[:,0,3] = fy01
    F[:,1,2] = fy10
    F[:,1,3] = fy11

    F[:,2,0] = fx00
    F[:,3,0] = fx01
    F[:,2,1] = fx10
    F[:,3,1] = fx11

    F[:,2,2] = fxy00
    F[:,2,3] = fxy01
    F[:,3,2] = fxy10
    F[:,3,3] = fxy11

  


    A = C @ F @ torch.t(C)
    #A = A *1e3
    dx , dy = x - x0.type(dtype) , y-y0.type(dtype) #local coordinates

    Xs = torch.stack([torch.ones_like(dx) , dx , dx**2 , dx**3 ] , 1) #(N , 4)
    Xs = torch.unsqueeze(Xs, dim=1) #(N ,1, 4)
    Ys = torch.stack([torch.ones_like(dy) , dy , dy**2 , dy**3 ] , 1) #(N, 4)
    Ys_t = torch.unsqueeze(Ys, dim=2) #(N, 4 , 1)

    l = torch.bmm(torch.bmm(Xs , A), Ys_t)

    fxy = l[:,0,0]

    return fxy