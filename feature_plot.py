import numpy as np
import matplotlib.pyplot as plt


import sys

import torch
from torch.nn import functional
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

omega = 1

from rewheel_main import PINN
pinn = PINN()


#pinn.load_state_dict(torch.load(f"./best_sofar.pt"))
pinn.load_state_dict(torch.load(f"./best_model_data_{omega}_2d.pt"))

zz = []
for Z in pinn.F_active:
    zz.append(Z.detach().cpu().numpy())


MIN = 100
for z in zz:
    if np.min(z) <= MIN:
        MIN = np.min(z)

MAX = 0
for z in zz:
    if np.max(z) >= MAX:
        MAX = np.max(z)

print(min , max)

n_f = pinn.n_features


fig, ax = plt.subplots(n_f,len(zz) , figsize = (n_f*3, len(zz)*3))
cm = plt.cm.bwr


for j , z in enumerate(zz):
    for i in range(n_f): 

        p = ax[i,j].pcolormesh(z[:,:,i], cmap=cm, vmin = MIN , vmax = MAX)
        ax[i,j].set_title(f'F{i}, level{j}')
        ax[i,j].set_box_aspect(1)
        plt.colorbar(p,ax=ax[i,j])

plt.savefig('features.png' , dpi = 300)
plt.show()